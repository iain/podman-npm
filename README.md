podman-npm: Run npm containerized, for some security against malicious packages

# Installation

Put the npm file into $PATH, e.g. ~/.local/bin.

Make the file executable, i.e. run: chmod +x npm.

# Usage

Use as you would normally use npm. e.g.:

  $ npm run dev

# License

MIT License